﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Debug = System.Diagnostics.Debug;

namespace GlslShadows
{
	public abstract class Shader
	{
		private int vertexShaderHandle, 
			fragmentShaderHandle, 
			shaderProgramHandle;

		private string vertexShaderPath, fragmentShaderPath;

		public int Program { get { return shaderProgramHandle; } }

		public Shader(string vertexShaderPath, string fragmentShaderPath)
		{
			this.vertexShaderPath = vertexShaderPath;
			this.fragmentShaderPath = fragmentShaderPath;
		}

		public virtual void LoadContent()
		{
			string vertexShaderSource = System.IO.File.ReadAllText(vertexShaderPath);
			string fragmentShaderSource = System.IO.File.ReadAllText(fragmentShaderPath);

			// Compile vertex shader
			vertexShaderHandle = GL.CreateShader(ShaderType.VertexShader);
			GL.ShaderSource(vertexShaderHandle, vertexShaderSource);
			GL.CompileShader(vertexShaderHandle);
			Debug.WriteLine(GL.GetShaderInfoLog(vertexShaderHandle));

			// Compile fragment shader
			fragmentShaderHandle = GL.CreateShader(ShaderType.FragmentShader);
			GL.ShaderSource(fragmentShaderHandle, fragmentShaderSource);
			GL.CompileShader(fragmentShaderHandle);
			Debug.WriteLine(GL.GetShaderInfoLog(fragmentShaderHandle));

			// Link program
			shaderProgramHandle = GL.CreateProgram();
			GL.AttachShader(shaderProgramHandle, vertexShaderHandle);
			GL.AttachShader(shaderProgramHandle, fragmentShaderHandle);
			GL.LinkProgram(shaderProgramHandle);
			Debug.WriteLine(GL.GetProgramInfoLog(shaderProgramHandle));
		}

		public virtual void UnloadContent()
		{
			GL.DeleteShader(vertexShaderHandle);
			GL.DeleteShader(fragmentShaderHandle);
			GL.DeleteProgram(shaderProgramHandle);
		}
	}
}
