﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using Debug = System.Diagnostics.Debug;

namespace GlslShadows
{
	public enum RenderMode
	{
		PCF,
		VSM,
		EVSM
	}

	public partial class ExampleProgram
	{
		GameWindow window;

		Geometry cube = new Geometry(@"content\cube.json");
		Geometry model = new Geometry(@"content\cow.json");

		Matrix4 projectionMatrix;
		Matrix4 orthographicMatrix = Matrix4.CreateOrthographicOffCenter(-25f, 25f, -25f, 25f, 0f, 100f);
		Vector3 cameraPosition = new Vector3(0f, 30f, 30f);
		Vector3 lightPosition = new Vector3(-30f, 30f, -30f);
		Vector3 cubePosition = new Vector3(0f, 0f, 0f);
		Vector3 cubeScale = new Vector3(15f, 0.25f, 15f);
		Vector3 model1Position = new Vector3(-1f, 7f, 7f);
		Vector3 model1Scale = new Vector3(2f);
		Vector3 model2Position = new Vector3(6f, 14f, 0f);
		Vector3 model2Scale = new Vector3(1f);

		RenderMode renderMode = RenderMode.PCF;
		bool rotateLight = true;
		bool exitProgram = false;

		public ExampleProgram(GameWindow window)
		{
			this.window = window;
			this.window.KeyUp += window_KeyUp;
		}

		public void Load()
		{
			// Set perspective
			float aspectRatio = window.ClientSize.Width / (float)(window.ClientSize.Height);
			Matrix4.CreatePerspectiveFieldOfView(45.0f * (float)(Math.PI / 180.0), aspectRatio, 0.1f, 200.0f, out projectionMatrix);

			// Load content
			cube.LoadContent();
			model.LoadContent();
			pcfDepthShader.LoadContent();
			pcfSceneShader.LoadContent();
			vsmDepthShader.LoadContent();
			vsmBlurShader.LoadContent();
			vsmSceneShader.LoadContent();
			evsmDepthShader.LoadContent();
			evsmBlurShader.LoadContent();
			evsmSceneShader.LoadContent();

			// Enable depth testing
			GL.Enable(EnableCap.DepthTest);
			GL.DepthFunc(DepthFunction.Less);
		}

		public void Unload()
		{
			cube.UnloadContent();
			model.UnloadContent();
			pcfDepthShader.UnloadContent();
			pcfSceneShader.UnloadContent();
			vsmDepthShader.UnloadContent();
			vsmBlurShader.UnloadContent();
			vsmSceneShader.UnloadContent();
			evsmDepthShader.UnloadContent();
			evsmBlurShader.UnloadContent();
			evsmSceneShader.UnloadContent();
		}

		public void UpdateState(double elapsedTotalSeconds)
		{
			double value = rotateLight ? elapsedTotalSeconds : 7.5;
			lightPosition.X = (float)Math.Sin(MathHelper.DegreesToRadians(10.0) * value) * 30f;
			lightPosition.Z = (float)Math.Cos(MathHelper.DegreesToRadians(10.0) * value) * -30f;
		}

		public void Draw(double elapsedTotalSeconds, double alpha)
		{
			switch (renderMode)
			{
				case RenderMode.PCF:
					DrawPCFDepthBuffer(pcfDepthShader);
					DrawScene(pcfSceneShader, pcfDepthShader);
					break;
				case RenderMode.VSM:
					DrawVSMDepthBuffer(vsmDepthShader);
					DrawVSMBlur(vsmBlurShader, vsmDepthShader);
					DrawScene(vsmSceneShader, vsmDepthShader);
					break;
				case RenderMode.EVSM:
					DrawVSMDepthBuffer(evsmDepthShader);
					DrawVSMBlur(evsmBlurShader, evsmDepthShader);
					DrawScene(evsmSceneShader, evsmDepthShader);
					break;
				default:
					break;
			}

			window.Context.SwapBuffers();
			ErrorHelper.DumpErrors();
		}

		public bool ProcessWindowEvents()
		{
			// Process events
			window.ProcessEvents();

			bool keepRunning = true;

			// Check window state
			if (window.IsClosing || !window.Exists) keepRunning = false;

			// Check escape key
			if (exitProgram) keepRunning = false;

			// Return
			return keepRunning;
		}

		void window_KeyUp(object sender, KeyboardKeyEventArgs e)
		{
			switch (e.Key)
			{
				case Key.Escape:
					exitProgram = true;
					break;
				case Key.Number1:
					renderMode = RenderMode.PCF;
					break;
				case Key.Number2:
					renderMode = RenderMode.VSM;
					break;
				case Key.Number3:
					renderMode = RenderMode.EVSM;
					break;
				case Key.Number4:
					rotateLight = !rotateLight;
					break;
				default:
					break;
			}
		}
	}
}
