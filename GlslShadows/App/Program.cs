﻿using System;

namespace GlslShadows
{
	static class Program
	{
		[STAThread]
		static void Main()
		{
			// Set minimum timer period
			uint minTimerRes, maxTimerRes, currentTimerRes;
			NativeMethods.NtQueryTimerResolution(out minTimerRes, out maxTimerRes, out currentTimerRes);
			NativeMethods.NtSetTimerResolution(minTimerRes, true, ref currentTimerRes);

			// Open a new GL context window
			using (GameWindow window = new GameWindow())
			{
				// Load example program
				ExampleProgram example = new ExampleProgram(window);
				example.Load();

				// Start fixed step loop
				window.Context.SwapInterval = 1;
				FixedStepLoop loop = new FixedStepLoop(60.0);

				// Run loop
				while (true)
				{
					if (example.ProcessWindowEvents())
					{
						loop.Execute(example.UpdateState, example.Draw);
						System.Threading.Thread.Sleep(1);
					}
					else
					{
						break;
					}
				}

				// Clean up
				example.Unload();
			}

			// Reset timer period
			NativeMethods.NtSetTimerResolution(currentTimerRes, true, ref currentTimerRes);
		}
	}
}
