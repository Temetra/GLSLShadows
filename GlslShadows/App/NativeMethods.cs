﻿using System.Security;
using System.Runtime.InteropServices;
using System.Diagnostics.CodeAnalysis;

namespace GlslShadows
{
	internal static class NativeMethods
	{
		[SuppressMessage("Microsoft.Interoperability", "CA1401:PInvokesShouldNotBeVisible")]
		[SuppressMessage("Microsoft.Security", "CA2118:ReviewSuppressUnmanagedCodeSecurityUsage")]
		[SuppressUnmanagedCodeSecurity]
		[DllImport("ntdll.dll", EntryPoint = "NtSetTimerResolution")]
		public static extern void NtSetTimerResolution(uint DesiredResolution, bool SetResolution, ref uint CurrentResolution);

		[SuppressMessage("Microsoft.Interoperability", "CA1401:PInvokesShouldNotBeVisible")]
		[SuppressMessage("Microsoft.Security", "CA2118:ReviewSuppressUnmanagedCodeSecurityUsage")]
		[SuppressUnmanagedCodeSecurity]
		[DllImport("ntdll.dll", SetLastError = true)]
		public static extern int NtQueryTimerResolution(out uint MinimumResolution, out uint MaximumResolution, out uint CurrentResolution);
	}
}
