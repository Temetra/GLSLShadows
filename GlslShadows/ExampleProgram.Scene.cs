﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using Debug = System.Diagnostics.Debug;

namespace GlslShadows
{
	public partial class ExampleProgram
	{
		void DrawScene(ISceneShader sceneShader, IDepthShader depthShader)
		{
			var cameraViewMatrix = Matrix4.LookAt(cameraPosition, Vector3.Zero, Vector3.UnitY);
			var lightViewMatrix = Matrix4.LookAt(lightPosition, Vector3.Zero, Vector3.UnitY);
			var lightViewPosition = Vector3.Transform(lightPosition, cameraViewMatrix);
			var biasMatrix = new Matrix4(
				0.5f, 0f, 0f, 0f,
				0f, 0.5f, 0f, 0f,
				0f, 0f, 0.5f, 0f,
				0.5f, 0.5f, 0.5f, 1f);

			// Clear
			GL.Viewport(0, 0, window.Width, window.Height);
			GL.ClearColor(Color.ForestGreen);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			// Set up shader
			GL.UseProgram(sceneShader.Program);
			GL.UniformMatrix4(sceneShader.ProjectionMat4, false, ref projectionMatrix);
			GL.Uniform3(sceneShader.LightPositionVec3, ref lightViewPosition);

			// Set shadow map
			GL.ActiveTexture(TextureUnit.Texture0);
			GL.BindTexture(TextureTarget.Texture2D, depthShader.DepthTexture);
			GL.Uniform1(sceneShader.ShadowMapInt, 0);

			// Draw cube
			var modelMatrix = Matrix4.CreateScale(cubeScale) * Matrix4.CreateTranslation(cubePosition);
			var modelViewMatrix = modelMatrix * cameraViewMatrix;
			var depthMatrix = modelMatrix * lightViewMatrix * orthographicMatrix * biasMatrix;
			GL.UniformMatrix4(sceneShader.ModelViewMat4, false, ref modelViewMatrix);
			GL.UniformMatrix4(sceneShader.BiasedDepthMat4, false, ref depthMatrix);
			cube.Draw();

			// Draw model 1
			modelMatrix = Matrix4.CreateRotationX(MathHelper.DegreesToRadians(-90f)) * Matrix4.CreateScale(model1Scale) * Matrix4.CreateTranslation(model1Position);
			modelViewMatrix = modelMatrix * cameraViewMatrix;
			depthMatrix = modelMatrix * lightViewMatrix * orthographicMatrix * biasMatrix;
			GL.UniformMatrix4(sceneShader.ModelViewMat4, false, ref modelViewMatrix);
			GL.UniformMatrix4(sceneShader.BiasedDepthMat4, false, ref depthMatrix);
			model.Draw();

			// Draw model 2
			modelMatrix = Matrix4.CreateRotationX(MathHelper.DegreesToRadians(-90f)) * Matrix4.CreateScale(model2Scale) * Matrix4.CreateTranslation(model2Position);
			modelViewMatrix = modelMatrix * cameraViewMatrix;
			depthMatrix = modelMatrix * lightViewMatrix * orthographicMatrix * biasMatrix;
			GL.UniformMatrix4(sceneShader.ModelViewMat4, false, ref modelViewMatrix);
			GL.UniformMatrix4(sceneShader.BiasedDepthMat4, false, ref depthMatrix);
			model.Draw();

			// Finished
			GL.UseProgram(0);
		}
	}
}
