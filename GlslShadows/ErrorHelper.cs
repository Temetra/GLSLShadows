﻿using OpenTK.Graphics.OpenGL;

namespace GlslShadows
{
	public static class ErrorHelper
	{
		static ErrorCode code, prevCode = ErrorCode.NoError;

		public static void DumpErrors()
		{
			while (true)
			{
				code = GL.GetError();
				if (code == ErrorCode.NoError) return;
				if (code != prevCode) System.Diagnostics.Debug.WriteLine("Error: " + code.ToString());
				prevCode = code;
			}
		}
	}
}
