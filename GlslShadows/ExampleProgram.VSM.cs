﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using Debug = System.Diagnostics.Debug;

namespace GlslShadows
{
	public partial class ExampleProgram
	{
		BlurShader vsmBlurShader = new BlurShader(@"content\BlurShader.vert", @"content\BlurShaderVSM.frag");
		VSMDepthShader vsmDepthShader = new VSMDepthShader(@"content\DepthShaderVSM.vert", @"content\DepthShaderVSM.frag");
		SceneShader vsmSceneShader = new SceneShader(@"content\SceneShader.vert", @"content\SceneShaderVSM.frag");

		VSMDepthShader evsmDepthShader = new VSMDepthShader(@"content\DepthShaderVSM.vert", @"content\DepthShaderEVSM.frag");
		BlurShader evsmBlurShader = new BlurShader(@"content\BlurShader.vert", @"content\BlurShaderEVSM.frag");
		SceneShader evsmSceneShader = new SceneShader(@"content\SceneShader.vert", @"content\SceneShaderEVSM.frag");

		void DrawVSMDepthBuffer(IDepthShader depthShader)
		{
			var lightViewMatrix = Matrix4.LookAt(lightPosition, Vector3.Zero, Vector3.UnitY);

			// Set up shader
			GL.UseProgram(depthShader.Program);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, depthShader.FrameBuffer);
			GL.Viewport(0, 0, depthShader.BufferSize, depthShader.BufferSize);
			GL.ClearColor(0f, 0f, 0f, 1f);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			// Draw cube
			var modelMatrix = Matrix4.CreateScale(cubeScale) * Matrix4.CreateTranslation(cubePosition);
			var depthMatrix = modelMatrix * lightViewMatrix * orthographicMatrix;
			GL.UniformMatrix4(depthShader.DepthMat4, false, ref depthMatrix);
			cube.Draw();

			// Draw model 1
			modelMatrix = Matrix4.CreateRotationX(MathHelper.DegreesToRadians(-90f)) * Matrix4.CreateScale(model1Scale) * Matrix4.CreateTranslation(model1Position);
			depthMatrix = modelMatrix * lightViewMatrix * orthographicMatrix;
			GL.UniformMatrix4(depthShader.DepthMat4, false, ref depthMatrix);
			model.Draw();

			// Draw model 2
			modelMatrix = Matrix4.CreateRotationX(MathHelper.DegreesToRadians(-90f)) * Matrix4.CreateScale(model2Scale) * Matrix4.CreateTranslation(model2Position);
			depthMatrix = modelMatrix * lightViewMatrix * orthographicMatrix;
			GL.UniformMatrix4(depthShader.DepthMat4, false, ref depthMatrix);
			model.Draw();

			// Finished
			GL.UseProgram(0);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
		}

		void DrawVSMBlur(IBlurShader blurShader, IDepthShader depthShader)
		{
			// Set up shader
			GL.UseProgram(blurShader.Program);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, blurShader.FrameBuffer);
			GL.Viewport(0, 0, blurShader.BufferSize, blurShader.BufferSize);
			GL.ClearColor(0f, 0f, 0f, 1f);
			GL.Clear(ClearBufferMask.DepthBufferBit | ClearBufferMask.ColorBufferBit);

			// Set texture to framebuffer result
			GL.ActiveTexture(TextureUnit.Texture0);
			GL.BindTexture(TextureTarget.Texture2D, depthShader.DepthTexture);
			GL.Uniform1(blurShader.TextureInt, 0);

			// Set scale for horizontal blur
			GL.Uniform2(blurShader.ScaleFloat, 1.0f / (float)depthShader.BufferSize, 0f);

			// Draw into temp blur buffer
			GL.BindVertexArray(blurShader.VertexArray);
			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 3);

			// Bind to source framebuffer
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, depthShader.FrameBuffer);
			GL.Viewport(0, 0, depthShader.BufferSize, depthShader.BufferSize);
			GL.Clear(ClearBufferMask.DepthBufferBit);

			// Set texture to blur result
			GL.BindTexture(TextureTarget.Texture2D, blurShader.BlurTexture);

			// Set scale for vertical blur
			GL.Uniform2(blurShader.ScaleFloat, 0f, 1.0f / (float)depthShader.BufferSize);

			// Draw into source buffer
			GL.BindVertexArray(blurShader.VertexArray);
			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 3);

			// Finished
			GL.UseProgram(0);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
		}
	}
}
