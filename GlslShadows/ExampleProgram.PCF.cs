﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using Debug = System.Diagnostics.Debug;

namespace GlslShadows
{
	public partial class ExampleProgram
	{
		PCFDepthShader pcfDepthShader = new PCFDepthShader(@"content\DepthShaderPCF.vert", @"content\DepthShaderPCF.frag");
		SceneShader pcfSceneShader = new SceneShader(@"content\SceneShader.vert", @"content\SceneShaderPCF.frag");

		void DrawPCFDepthBuffer(IDepthShader depthShader)
		{
			var lightViewMatrix = Matrix4.LookAt(lightPosition, Vector3.Zero, Vector3.UnitY);

			// Set up shader
			GL.UseProgram(depthShader.Program);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, depthShader.FrameBuffer);
			GL.Viewport(0, 0, depthShader.BufferSize, depthShader.BufferSize);
			GL.ClearColor(0f, 0f, 0f, 1f);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			GL.Enable(EnableCap.PolygonOffsetFill);
			GL.PolygonOffset(1.1f, 0f);

			// Draw cube
			var modelMatrix = Matrix4.CreateScale(cubeScale) * Matrix4.CreateTranslation(cubePosition);
			var depthMatrix = modelMatrix * lightViewMatrix * orthographicMatrix;
			GL.UniformMatrix4(depthShader.DepthMat4, false, ref depthMatrix);
			cube.Draw();

			// Draw model 1
			modelMatrix = Matrix4.CreateRotationX(MathHelper.DegreesToRadians(-90f)) * Matrix4.CreateScale(model1Scale) * Matrix4.CreateTranslation(model1Position);
			depthMatrix = modelMatrix * lightViewMatrix * orthographicMatrix;
			GL.UniformMatrix4(depthShader.DepthMat4, false, ref depthMatrix);
			model.Draw();

			// Draw model 2
			modelMatrix = Matrix4.CreateRotationX(MathHelper.DegreesToRadians(-90f)) * Matrix4.CreateScale(model2Scale) * Matrix4.CreateTranslation(model2Position);
			depthMatrix = modelMatrix * lightViewMatrix * orthographicMatrix;
			GL.UniformMatrix4(depthShader.DepthMat4, false, ref depthMatrix);
			model.Draw();

			// Finished
			GL.UseProgram(0);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
			GL.Disable(EnableCap.PolygonOffsetFill);
		}
	}
}
