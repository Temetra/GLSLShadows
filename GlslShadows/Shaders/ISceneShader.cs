﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using Debug = System.Diagnostics.Debug;

namespace GlslShadows
{
	public interface ISceneShader
	{
		int Program { get; }
		int ProjectionMat4 { get; }
		int ModelViewMat4 { get; }
		int BiasedDepthMat4 { get; }
		int LightPositionVec3 { get; }
		int ShadowMapInt { get; }
	}
}
