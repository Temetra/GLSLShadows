﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using Debug = System.Diagnostics.Debug;

namespace GlslShadows
{
	public class PCFDepthShader : Shader, IDepthShader
	{
		private int frameBuffer, depthTexture;

		public int DepthMat4 { get; private set; }
		public int BufferSize { get { return 1024; } }
		public int FrameBuffer { get { return frameBuffer; } }
		public int DepthTexture { get { return depthTexture; } }

		public PCFDepthShader(string vertexShaderPath, string fragmentShaderPath)
			: base(vertexShaderPath, fragmentShaderPath)
		{
		}

		public override void LoadContent()
		{
			base.LoadContent();

			// Bind attrib locations
			GL.BindAttribLocation(Program, 0, "in_position");

			// Get uniforms
			DepthMat4 = GL.GetUniformLocation(Program, "depth_matrix");

			// Set up render target
			GL.GenFramebuffers(1, out frameBuffer);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, frameBuffer);

			// Set up depth texture
			GL.GenTextures(1, out depthTexture);
			GL.BindTexture(TextureTarget.Texture2D, depthTexture);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMagFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareMode, (int)TextureCompareMode.CompareRefToTexture);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareFunc, (int)All.Lequal);
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.DepthComponent, BufferSize, BufferSize, 0, PixelFormat.DepthComponent, PixelType.UnsignedByte, IntPtr.Zero);

			GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, depthTexture, 0);
			GL.DrawBuffer(DrawBufferMode.None);
			GL.ReadBuffer(ReadBufferMode.None);

			// Check
			var status = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
			if (status == FramebufferErrorCode.FramebufferComplete) System.Diagnostics.Debug.WriteLine("PCFDepthShader buffer complete");

			// Done
			GL.BindTexture(TextureTarget.Texture2D, 0);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
		}

		public override void UnloadContent()
		{
			base.UnloadContent();
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
			GL.DeleteTexture(depthTexture);
			GL.DeleteFramebuffer(frameBuffer);
		}
	}
}
