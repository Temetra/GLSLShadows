﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using Debug = System.Diagnostics.Debug;

namespace GlslShadows
{
	public class SceneShader : Shader, ISceneShader
	{
		public int ProjectionMat4 { get; private set; }
		public int ModelViewMat4 { get; private set; }
		public int BiasedDepthMat4 { get; private set; }
		public int LightPositionVec3 { get; private set; }
		public int ShadowMapInt { get; private set; }

		public SceneShader(string vertexShaderPath, string fragmentShaderPath)
			: base(vertexShaderPath, fragmentShaderPath)
		{
		}

		public override void LoadContent()
		{
			base.LoadContent();

			// Bind attrib locations
			GL.BindAttribLocation(Program, 0, "in_position");
			GL.BindAttribLocation(Program, 1, "in_normal");

			// Get uniforms
			ProjectionMat4 = GL.GetUniformLocation(Program, "projection_matrix");
			ModelViewMat4 = GL.GetUniformLocation(Program, "modelview_matrix");
			BiasedDepthMat4 = GL.GetUniformLocation(Program, "biased_depth_matrix");
			LightPositionVec3 = GL.GetUniformLocation(Program, "light_position");
			ShadowMapInt = GL.GetUniformLocation(Program, "shadow_sampler");
		}

		public override void UnloadContent()
		{
			base.UnloadContent();
		}
	}
}
