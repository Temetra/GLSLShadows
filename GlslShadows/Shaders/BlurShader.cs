﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using Debug = System.Diagnostics.Debug;

namespace GlslShadows
{
	public class BlurShader : Shader, IBlurShader
	{
		private int frameBuffer, vertexArray, blurTexture, textureInt, scaleFloat;
		public int BufferSize { get { return 512; } }
		public int FrameBuffer { get { return frameBuffer; } }
		public int VertexArray { get { return vertexArray; } }
		public int BlurTexture { get { return blurTexture; } }
		public int TextureInt { get { return textureInt; } }
		public int ScaleFloat { get { return scaleFloat; } }

		public BlurShader(string vertexShaderPath, string fragmentShaderPath)
			: base(vertexShaderPath, fragmentShaderPath)
		{
		}

		public override void LoadContent()
		{
			base.LoadContent();

			// Bind attrib locations
			GL.BindAttribLocation(Program, 0, "in_position");

			// Get uniforms
			textureInt = GL.GetUniformLocation(Program, "texture");
			scaleFloat = GL.GetUniformLocation(Program, "scale");

			// Set up blur texture
			GL.GenTextures(1, out blurTexture);
			GL.BindTexture(TextureTarget.Texture2D, blurTexture);
			int max_aniso = 0;
			GL.GetInteger((GetPName)ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt, out max_aniso);
			GL.TexParameter(TextureTarget.Texture2D, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, max_aniso);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba32f, BufferSize, BufferSize, 0, PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero);
			GL.BindTexture(TextureTarget.Texture2D, 0);

			// Set up render target
			GL.GenFramebuffers(1, out frameBuffer);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, frameBuffer);
			GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, blurTexture, 0);

			// Check
			var status = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
			if (status == FramebufferErrorCode.FramebufferComplete) System.Diagnostics.Debug.WriteLine("VSMBlurShader buffer complete");

			// Clear
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

			// Generate dummy vertex array
			GL.GenVertexArrays(1, out vertexArray);
			GL.BindVertexArray(vertexArray);
			GL.BindVertexArray(0);

			// Done
			GL.BindTexture(TextureTarget.Texture2D, 0);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
		}

		public override void UnloadContent()
		{
			base.UnloadContent();
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
			GL.DeleteTexture(blurTexture);
			GL.DeleteFramebuffer(frameBuffer);
			GL.DeleteVertexArray(vertexArray);
		}
	}
}
