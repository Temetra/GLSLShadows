﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using Debug = System.Diagnostics.Debug;

namespace GlslShadows
{
	public interface IDepthShader
	{
		int Program { get; }
		int DepthTexture { get; }
		int DepthMat4 { get; }
		int BufferSize { get; }
		int FrameBuffer { get; }
	}
}
