﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using Debug = System.Diagnostics.Debug;

namespace GlslShadows
{
	public class VSMDepthShader : Shader, IDepthShader
	{
		private int frameBuffer, renderBuffer, depthTexture;

		public int DepthMat4 { get; protected set; }
		public int BufferSize { get { return 1024; } }
		public int FrameBuffer { get { return frameBuffer; } }
		public int DepthTexture { get { return depthTexture; } }

		public VSMDepthShader(string vertexShaderPath, string fragmentShaderPath)
			: base(vertexShaderPath, fragmentShaderPath)
		{
		}

		public override void LoadContent()
		{
			base.LoadContent();

			// Bind attrib locations
			GL.BindAttribLocation(Program, 0, "in_position");

			// Get uniforms
			DepthMat4 = GL.GetUniformLocation(Program, "depth_matrix");

			// Set up depth texture
			GL.GenTextures(1, out depthTexture);
			GL.BindTexture(TextureTarget.Texture2D, depthTexture);
			int max_aniso = 0;
			GL.GetInteger((GetPName)ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt, out max_aniso);
			GL.TexParameter(TextureTarget.Texture2D, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, max_aniso);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba32f, BufferSize, BufferSize, 0, PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero);
			GL.BindTexture(TextureTarget.Texture2D, 0);

			// Render buffer
			GL.GenRenderbuffers(1, out renderBuffer);
			GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, renderBuffer);
			GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent, BufferSize, BufferSize);
			GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, 0);

			// Set up render target
			GL.GenFramebuffers(1, out frameBuffer);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, frameBuffer);
			GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, depthTexture, 0);
			GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, renderBuffer);

			// Check
			var status = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
			if (status == FramebufferErrorCode.FramebufferComplete) System.Diagnostics.Debug.WriteLine("VSMDepthShader buffer complete");

			// Done
			GL.BindTexture(TextureTarget.Texture2D, 0);
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
		}

		public override void UnloadContent()
		{
			base.UnloadContent();
			GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
			GL.DeleteTexture(depthTexture);
			GL.DeleteFramebuffer(frameBuffer);
			GL.DeleteRenderbuffer(renderBuffer);
		}
	}
}
