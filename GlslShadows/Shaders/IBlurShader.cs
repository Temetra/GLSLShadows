﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using Debug = System.Diagnostics.Debug;

namespace GlslShadows
{
	public interface IBlurShader
	{
		int Program { get; }
		int BufferSize { get; }
		int FrameBuffer { get; }
		int VertexArray { get; }
		int BlurTexture { get; }
		int TextureInt { get; }
		int ScaleFloat { get; }
	}
}
