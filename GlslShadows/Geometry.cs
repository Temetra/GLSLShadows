﻿using System;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace GlslShadows
{
	public struct GeometrySource
	{
		public float[] Positions;
		public float[] Normals;
		public float[] TexCoords;
		public int[] Indices;
	}

	public class Geometry
	{
		private string path;
		private int indicesLength = 0;
		private int 
			positionVboHandle, 
			normalVboHandle,
			texcoordVboHandle,
			eboHandle, 
			vaoHandle;

		public Geometry(string path)
		{
			this.path = path;
		}

		public void LoadContent()
		{
			GeometrySource data;
			
			// Load data from json
			var serializer = Newtonsoft.Json.JsonSerializer.CreateDefault();
			var textReader = new System.IO.StreamReader(path);
			using (var jsonReader = new Newtonsoft.Json.JsonTextReader(textReader))
			{
				data = serializer.Deserialize<GeometrySource>(jsonReader);
			}

			// Vertices
			GL.GenBuffers(1, out positionVboHandle);
			GL.BindBuffer(BufferTarget.ArrayBuffer, positionVboHandle);
			GL.BufferData<float>(BufferTarget.ArrayBuffer,
				new IntPtr(sizeof(float) * data.Positions.Length),
				data.Positions, BufferUsageHint.StaticDraw);

			// Normals
			GL.GenBuffers(1, out normalVboHandle);
			GL.BindBuffer(BufferTarget.ArrayBuffer, normalVboHandle);
			GL.BufferData<float>(BufferTarget.ArrayBuffer,
				new IntPtr(sizeof(float) * data.Normals.Length),
				data.Normals, BufferUsageHint.StaticDraw);

			// Texcoords
			GL.GenBuffers(1, out texcoordVboHandle);
			GL.BindBuffer(BufferTarget.ArrayBuffer, texcoordVboHandle);
			GL.BufferData<float>(BufferTarget.ArrayBuffer,
				new IntPtr(sizeof(float) * data.TexCoords.Length),
				data.TexCoords, BufferUsageHint.StaticDraw);

			// Indices
			GL.GenBuffers(1, out eboHandle);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, eboHandle);
			GL.BufferData(BufferTarget.ElementArrayBuffer,
				new IntPtr(sizeof(uint) * data.Indices.Length),
				data.Indices, BufferUsageHint.StaticDraw);
			indicesLength = data.Indices.Length;

			// Clear
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);

			// Set vertex array object
			GL.GenVertexArrays(1, out vaoHandle);
			GL.BindVertexArray(vaoHandle);

			GL.EnableVertexAttribArray(0);
			GL.BindBuffer(BufferTarget.ArrayBuffer, positionVboHandle);
			GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, sizeof(float) * 3, 0);

			GL.EnableVertexAttribArray(1);
			GL.BindBuffer(BufferTarget.ArrayBuffer, normalVboHandle);
			GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, sizeof(float) * 3, 0);

			GL.EnableVertexAttribArray(2);
			GL.BindBuffer(BufferTarget.ArrayBuffer, texcoordVboHandle);
			GL.VertexAttribPointer(2, 2, VertexAttribPointerType.Float, false, sizeof(float) * 2, 0);

			GL.BindBuffer(BufferTarget.ElementArrayBuffer, eboHandle);
			GL.BindVertexArray(0);
		}

		public void UnloadContent()
		{
			GL.DeleteVertexArrays(1, ref vaoHandle);
			GL.DeleteBuffers(4, new int[] { positionVboHandle, normalVboHandle, texcoordVboHandle, eboHandle });
		}

		public void Draw()
		{
			GL.BindVertexArray(vaoHandle);
			GL.DrawElements(PrimitiveType.Triangles, indicesLength, DrawElementsType.UnsignedInt, IntPtr.Zero);
		}
	}
}
