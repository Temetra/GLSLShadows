﻿#version 150 core

uniform mat4 depth_matrix;
in vec3 in_position;

void main(void) 
{
	gl_Position = depth_matrix * vec4(in_position, 1.0);
}
