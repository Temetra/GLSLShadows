﻿#version 150 core

in vec4 vs_position;
out vec4 out_frag_color;

void main(void)
{
	vec2 moments = vec2(vs_position.z / vs_position.w, 0.0);
	moments.x = moments.x * 0.5 + 0.5;
	moments.y = moments.x * moments.x;

	// split precision
	float factor = 1.0 / 256.0;
	vec2 intpart;  
	vec2 fracpart = modf(moments * 256.0, intpart);
	out_frag_color = vec4(intpart * factor, fracpart);
}
