﻿#version 150 core

uniform sampler2D texture_sampler;
in vec2 vs_texcoord;
in vec2 vs_blurcoords[5];
out vec4 out_frag_color;

void main(void)
{
	vec4 value = vec4(0.0);
	value += texture2D(texture_sampler, vs_blurcoords[0]).rbga * 0.204164;
	value += texture2D(texture_sampler, vs_blurcoords[1]).rbga * 0.304005;
	value += texture2D(texture_sampler, vs_blurcoords[2]).rbga * 0.304005;
	value += texture2D(texture_sampler, vs_blurcoords[3]).rbga * 0.093913;
	value += texture2D(texture_sampler, vs_blurcoords[4]).rbga * 0.093913;
	out_frag_color = value;
}
