﻿#version 150 core

uniform sampler2D shadow_sampler;
in vec3 vs_normal;
in vec3 vs_lightdir;
in vec4 vs_shadowcoord;
out vec4 out_frag_color;

float Chebyshev(in vec2 moments, in float mean, in float minVariance)
{
    if (mean <= moments.x) return 1.0;
	float variance = moments.y - (moments.x * moments.x);
	variance = max(variance, minVariance);
	float d = mean - moments.x;
	return smoothstep(0.20, 1.0, variance / (variance + d*d));
}

float vsm()
{
	vec4 coords = vs_shadowcoord / vs_shadowcoord.w;
	vec4 colour = texture(shadow_sampler, coords.xy);

	// Recombine precision
	vec2 moments = (colour.zw * (1.0/256.0) + colour.xy);
	
	return Chebyshev(moments, coords.z, 0.00001);
}

void main(void)
{
	vec3 lightdir = normalize(vs_lightdir);
	float NdotL = dot(vs_normal, lightdir);

	vec3 shadowcoord = vs_shadowcoord.xyz/vs_shadowcoord.w;
	float visibility = vsm();

	float colour = NdotL * visibility;
	out_frag_color = vec4(colour, colour, colour, 1.0);
}
