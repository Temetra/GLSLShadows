﻿#version 150 core

uniform sampler2D texture_sampler;
in vec2 vs_texcoord;
in vec2 vs_blurcoords[5];
out vec4 out_frag_color;

vec2 recombine(in int idx)  
{  
	vec4 value = texture2D(texture_sampler, vs_blurcoords[idx]);
	return (value.zw * (1.0 / 256.0) + value.xy);
}  

void main(void)
{
	vec2 colour = vec2(0.0);

	// precision is split, so we recombine first
	colour += recombine(0) * 0.204164;
	colour += recombine(1) * 0.304005;
	colour += recombine(2) * 0.304005;
	colour += recombine(3) * 0.093913;
	colour += recombine(4) * 0.093913;

	// split precision again
	float factor = 1.0 / 256.0;
	vec2 intpart;  
	vec2 fracpart = modf(colour * 256.0, intpart);
	out_frag_color = vec4(intpart * factor, fracpart);
}
