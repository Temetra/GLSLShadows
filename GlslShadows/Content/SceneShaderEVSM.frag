﻿#version 150 core

uniform sampler2D shadow_sampler;
in vec3 vs_normal;
in vec3 vs_lightdir;
in vec4 vs_shadowcoord;
out vec4 out_frag_color;

float Chebyshev(in vec2 moments, in float mean, in float minVariance)
{
    if (mean <= moments.x) return 1.0;
	float variance = moments.y - (moments.x * moments.x);
	variance = max(variance, minVariance);
	float d = mean - moments.x;
	return smoothstep(0.20, 1.0, variance / (variance + d*d));
}

float evsm()
{
	vec4 coords = vs_shadowcoord / vs_shadowcoord.w;
	float depth = coords.z;
	depth = 2.0 * depth - 1.0;
	vec2 exponents = vec2(20.0, 10.0);
	float pos =  exp( exponents.x * depth);
	float neg = -exp(-exponents.y * depth);
	vec2 warpedDepth = vec2(pos, neg);

	vec4 occluder = texture(shadow_sampler, coords.xy);
    vec2 depthScale = 0.01 * exponents * warpedDepth;
    vec2 minVariance = depthScale * depthScale;

    float posContrib = Chebyshev(occluder.xz, warpedDepth.x, minVariance.x);
    float negContrib = Chebyshev(occluder.yw, warpedDepth.y, minVariance.y);

	return min(posContrib, negContrib);
}

void main(void)
{
	vec3 lightdir = normalize(vs_lightdir);
	float NdotL = dot(vs_normal, lightdir);

	vec3 shadowcoord = vs_shadowcoord.xyz/vs_shadowcoord.w;
	float visibility = evsm();

	float colour = NdotL * visibility;
	out_frag_color = vec4(colour, colour, colour, 1.0);
}
