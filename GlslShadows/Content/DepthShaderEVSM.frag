﻿#version 150 core

in vec4 vs_position;
out vec4 out_frag_color;

void main(void)
{
	float depth = gl_FragCoord.z;
	depth = 2.0 * depth - 1.0;
	vec2 exponents = vec2(20.0, 10.0);
	float pos =  exp( exponents.x * depth);
	float neg = -exp(-exponents.y * depth);
	vec2 warpedDepth = vec2(pos, neg);
	out_frag_color = vec4(warpedDepth.xy, warpedDepth.xy * warpedDepth.xy);
}
