﻿#version 150 core

uniform sampler2DShadow shadow_sampler;

in vec3 vs_normal;
in vec3 vs_lightdir;
in vec4 vs_shadowcoord;

const float shadowWidth = 1.0/500.0;
const int shadowSamples = 16;
const float invShadowSamples = 1.0/float(shadowSamples);
const vec2 poissonDisk[16] = vec2[]( 
	vec2(-0.7018766, -0.4510135),
	vec2(-0.1996162, -0.5232714),
	vec2(-0.3140921, -0.027881),
	vec2(-0.2628016, -0.9470145),
	vec2(-0.7987705, 0.07178555),
	vec2(0.2947877, 0.1789216),
	vec2(-0.06666037, 0.3846774),
	vec2(0.2768719, -0.4643578),
	vec2(0.3531616, -0.9186342),
	vec2(0.2469831, 0.781698),
	vec2(0.5462195, 0.5256281),
	vec2(0.6603884, -0.1872602),
	vec2(-0.4917641, 0.7179955),
	vec2(-0.1598853, 0.9389448),
	vec2(0.6425261, -0.6031773),
	vec2(0.8466412, 0.2667517)
);

out vec4 out_frag_color;

void main(void)
{
	vec3 lightdir = normalize(vs_lightdir);
	float NdotL = dot(vs_normal, lightdir);

	vec3 shadowcoord = vs_shadowcoord.xyz/vs_shadowcoord.w;
	float visibility = 0.0;
	for (int i = 0; i < shadowSamples; i++)
	{
		visibility += texture(shadow_sampler, vec3(poissonDisk[i] * shadowWidth + shadowcoord.xy, shadowcoord.z - 0.005)) * invShadowSamples;
	}

	float colour = NdotL * visibility;
	out_frag_color = vec4(colour, colour, colour, 1.0);
}
