﻿#version 150 core

out float out_frag_color;

void main(void)
{
	out_frag_color = gl_FragCoord.z;
}
