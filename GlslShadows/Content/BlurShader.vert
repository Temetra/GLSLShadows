﻿#version 150 core

const vec2 verts[3] = vec2[](vec2(-1, -1), vec2(3, -1), vec2(-1, 3));
const vec2 uvs[3] = vec2[](vec2(0, -2), vec2(2, -2), vec2(0, 0));

uniform vec2 scale;
out vec2 vs_texcoord;
out vec2 vs_blurcoords[5];

void main(void) 
{
	vs_texcoord = uvs[gl_VertexID];
	gl_Position = vec4(verts[gl_VertexID], 0.0, 1.0);

	vs_blurcoords[0] = vs_texcoord.xy;
	vs_blurcoords[1] = vs_texcoord.xy + scale * 1.407333;
	vs_blurcoords[2] = vs_texcoord.xy - scale * 1.407333;
	vs_blurcoords[3] = vs_texcoord.xy + scale * 3.294215;
	vs_blurcoords[4] = vs_texcoord.xy - scale * 3.294215;
}
