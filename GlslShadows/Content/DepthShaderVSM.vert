﻿#version 150 core

uniform mat4 depth_matrix;
in vec3 in_position;
out vec4 vs_position;

void main(void) 
{
	vs_position = depth_matrix * vec4(in_position, 1.0);
	gl_Position = vs_position;
}
