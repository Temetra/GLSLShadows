﻿#version 150 core

uniform mat4 projection_matrix;
uniform mat4 modelview_matrix;
uniform mat4 biased_depth_matrix;
uniform vec3 light_position;

in vec3 in_position;
in vec3 in_normal;

out vec3 vs_normal;
out vec3 vs_lightdir;
out vec4 vs_shadowcoord;

void main(void)
{
	mat3 normal_matrix = mat3(transpose(inverse(modelview_matrix)));
	vec4 mvPosition = modelview_matrix * vec4(in_position, 1.0);
	vs_normal = normalize(normal_matrix * in_normal);
	vs_lightdir = light_position - mvPosition.xyz;
	vs_shadowcoord = biased_depth_matrix * vec4(in_position, 1.0);
	gl_Position = projection_matrix * mvPosition;
}
